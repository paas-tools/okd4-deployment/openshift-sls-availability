import logging
import os

# logging setup
# choose one of CRITICAL, ERROR, WARNING, INFO, DEBUG
# https://docs.python.org/3/library/logging.html#levels
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')
LOG_FORMAT: str = '[%(asctime)-15s] %(levelname)-8s %(funcName)s: %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)

# configuration values for connection to the Openshift cluster
EXTERNAL_OPENSHIFT_ENDPOINT: str = os.environ.get('EXTERNAL_OPENSHIFT_ENDPOINT')
OPENSHIFT_TOKEN: str = os.environ.get('OPENSHIFT_TOKEN')
PROMETHEUS_ENDPOINT: str = os.environ.get('PROMETHEUS_ENDPOINT', 'https://prometheus-k8s.openshift-monitoring.svc.cluster.local:9091')
TLS_VERIFY: str = os.environ.get('TLS_VERIFY', '/var/run/secrets/kubernetes.io/serviceaccount/service-ca.crt')

# configuration values for connection to central monitoring reporting endpoint
SLS_ENDPOINT: str = os.environ.get('SLS_ENDPOINT', 'http://monit-metrics.cern.ch:10012/')

# miscellaneous configuration values
DRY_RUN: str = os.environ.get('DRY_RUN', 'false')

# Prometheus queries
PROMETHEUS_QUERIES_NUMERIC_VALUES: dict = {
    'number_of_schedulable_nodes': 'count(min(kube_node_spec_unschedulable==0)by(node))',
    'number_of_running_pods': 'count(kube_pod_status_phase{phase=~"Running|Terminating"})',
    'total_memory_used_mb': 'sum(container_memory_usage_bytes{name=~"k8s_.+"})/1000000',
    'router_bandwidth': '(sum(rate(haproxy_backend_bytes_in_total[10m])) + sum(rate(haproxy_backend_bytes_out_total[10m])))*8/1000000',
    'number_of_user_projects': 'count(kube_namespace_labels{label_okd_cern_ch_user_project="true"})',
    'number_cephfs_pvs': 'count(kube_persistentvolume_info{storageclass=~"cephfs.*"})'
}

# If any of the capacity alarms in prometheus is firing, mark service as degraded.
# This usually means the current schedulable, standard and ready nodes are not sufficient
# to run the service.
OPENSHIFT_SLS_DEGRADED_QUERY: str = 'count(ALERTS{alertname=~"OpenshiftStandardNodesAllocatableMemory|OpenshiftStandardNodesAllocatableCpu", alertstate="firing"}) OR on() vector(0)'
