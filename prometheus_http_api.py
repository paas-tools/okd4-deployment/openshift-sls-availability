import logging

import requests

class PrometheusAPI():
    """Python binding for the HTTP Prometheus API"""
    def __init__(self, endpoint: str, token: str, tls_verify: str):
        self.endpoint = endpoint
        # instatiate a session so we can re-use the connection to Prometheus
        # https://requests.readthedocs.io/en/master/user/advanced/?highlight=session#session-objects
        self.session = requests.Session()
        # set authentication headers used for all requests on the session
        self.session.headers.update({'Authorization': 'Bearer ' + token})
        if tls_verify == "false":
            self.session.verify = False
        else:
            # assume that tls_verify contains the path to the CA bundle
            self.session.verify = tls_verify

    def query(self, query):
        url: str = self.endpoint + '/api/v1/query'
        logging.debug(f"Executing query '{query}' against '{url}'")
        resp = self.session.get(url, timeout=15, params={'query': query})
        logging.debug(resp.request.headers)
        # logging.debug(resp.text)
        resp.raise_for_status()
        return resp

    def single_value_query(self, query):
        """
        Extract the single value from the JSON structure of the Prometheus API
        """
        resp = self.query(query).json()
        # Extract the value from the Prometheus Json structure
        # The response looks like:
        # {'status': 'success', 'data': {'resultType': 'vector', 'result': [{'value': [1501595028.608, '205'], 'metric': {}}]}}
        try:
            return resp['data']['result'][0]['value'][-1]
        except IndexError:
            logging.error('Response query does not have a value in the expected field. Returning None')
            return None
