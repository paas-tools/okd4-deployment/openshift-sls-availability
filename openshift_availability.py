#!/usr/bin/env python3

import json
import logging
import time

import requests
from cernservicexml import Status

from config import PROMETHEUS_QUERIES_NUMERIC_VALUES, OPENSHIFT_SLS_DEGRADED_QUERY, EXTERNAL_OPENSHIFT_ENDPOINT, OPENSHIFT_TOKEN, PROMETHEUS_ENDPOINT, SLS_ENDPOINT, DRY_RUN, TLS_VERIFY
from prometheus_http_api import PrometheusAPI

def obtain_openshift_availability(prometheus_api: object, openshift_endpoint: str, token: str):
    """
    Criteria:
    - Degraded
        - The ratio of free memory in standard, ready and schedulable nodes is less than a given threshold
    - Unavailable
        - Kubernetes API unavailable via Infra nodes (api.paas.okd.cern.ch)

    Returns a tuple containing the cernservicexml.Status and a dict of numeric_values
    in the format of {query: query_result}
    """

    status = Status.available
    numeric_values = {name: float(prometheus_api.single_value_query(query))
                      for name, query in PROMETHEUS_QUERIES_NUMERIC_VALUES.items()}

    # DEGRADED QUERY
    # Check ratio of free memory in standard, ready and schedulable nodes
    value = prometheus_api.single_value_query(OPENSHIFT_SLS_DEGRADED_QUERY)
    if value and int(value) >= 1:
        logging.info(
            'There are capacity alarms firing, service is DEGRADED ...')
        status = Status.degraded
    else:
        logging.info(
            'There are no capacity alarms firing, service is AVAILABLE ...')

    # UNAVAILABLE QUERY
    # Check if the Kubernetes API is available (via ingress routers running on infra nodes)
    try:
        resp = requests.get(openshift_endpoint + '/apis/user.openshift.io/v1/users/~',
                            headers={"Authorization": "Bearer " + token},
                            verify=True, # set this to False when connecting a dev cluster
                            timeout=10,
                            )
        if resp.status_code != 200:
            logging.info(
                'Openshift API returned %s, marking service as UNAVAILABLE' % resp.status_code)
            status = Status.unavailable
        else:
            logging.info('Openshift API returned %s, successful check' %
                         resp.status_code)
    except requests.exceptions.RequestException as ex:  # This is the correct syntax
        logging.info('Cannot contact the OpenShift API: %s ' % repr(ex))
        status = Status.unavailable

    return status, numeric_values

def generate_service_status_json(status: str) -> dict:
    """
    Given the current service status, returns the service status document expected by the SLS endpoint.
    """
    timestamp = int(round(time.time() * 1000))
    document = [{
        "producer": "openshift",
        "type": "availability",
        "serviceid": "paas",
        "service_status": status,
        "contact": "https://cern.service-now.com/service-portal/?id=service_element&name=PaaS-Web-App",
        "webpage": "https://paas.cern.ch",
        "timestamp": timestamp
    }]
    return document

def generate_service_metrics_json(prometheus_api: object, openshift_endpoint: str, token: str) -> tuple[list, str]:
    """
    Queries the Prometheus API and returns a formatted document as expected by the SLS endpoint.
    Returns a dict and string indicating the service status (AVAILABLE, DEGRADED).
    """
    status, numeric_values = obtain_openshift_availability(prometheus_api, openshift_endpoint, token)

    document = [{
        "producer": "openshift",
        "type": "kpi",
        "number_of_schedulable_nodes": numeric_values['number_of_schedulable_nodes'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_of_schedulable_nodes"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "number_of_running_pods": numeric_values['number_of_running_pods'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_of_running_pods"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "total_memory_used_mb": numeric_values['total_memory_used_mb'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["total_memory_used_mb"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "router_bandwidth": numeric_values['router_bandwidth'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["router_bandwidth"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "number_of_user_projects": numeric_values['number_of_user_projects'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_of_user_projects"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "number_cephfs_pvs": numeric_values['number_cephfs_pvs'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_cephfs_pvs"]
        }
    ]

    return document, status

def publish_metrics(sls_endpoint: str, document: dict):
    text = json.dumps(document)
    logging.debug(f"Publishing document to {sls_endpoint}: {text}")
    response = requests.post(sls_endpoint,
                             headers={"Content-Type": "application/json; charset=UTF-8"},
                             data=text,
                             )

    # Throw an exception if HTTP status code is >= 400
    # https://requests.readthedocs.io/en/latest/api/#requests.Response.raise_for_status
    response.raise_for_status()
    logging.info("Successfully published document to {sls_endpoint}")

def run_openshift_availability():
    dry_run: bool = False
    if DRY_RUN != "false":
        dry_run = True
        logging.info('Running in dry-run mode, metrics will not be published')

    logging.info(f"Using prometheus endpoint '{PROMETHEUS_ENDPOINT}'")
    logging.info(f"Using openshift endpoint '{EXTERNAL_OPENSHIFT_ENDPOINT}'")
    logging.info(f"Using SLS endpoint '{SLS_ENDPOINT}'")

    token: str
    if not OPENSHIFT_TOKEN:
        raise Exception(f"No 'OPENSHIFT_TOKEN' found in environment")

    if OPENSHIFT_TOKEN.startswith('/'):
        logging.info(f"Loading OPENSHIFT_TOKEN from file '{OPENSHIFT_TOKEN}'")
        with open(OPENSHIFT_TOKEN, 'r') as f:
            token = f.read()
    else:
        token = OPENSHIFT_TOKEN

    # create a new connection wrapper for talking to Prometheus
    prometheus_api = PrometheusAPI(PROMETHEUS_ENDPOINT, token, TLS_VERIFY)

    # fetch metrics from prometheus
    # this function also generates the applicable service status (AVAILABLE, DEGRADED)
    metrics_document, status = generate_service_metrics_json(prometheus_api, EXTERNAL_OPENSHIFT_ENDPOINT, token)
    status_document = generate_service_status_json(status)

    # Output metrics on stdout (if dry-run) or send them to Monit
    if dry_run:
        print(f"Service status: {json.dumps(status_document)}")
        print(f"Service metrics: {json.dumps(metrics_document)}")
    else:
        publish_metrics(SLS_ENDPOINT, status_document)
        publish_metrics(SLS_ENDPOINT, metrics_document)

    logging.info("Done.")

if __name__ == "__main__":
    run_openshift_availability()
