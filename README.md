OpenShift availability
=================

This application indicates the status of "PaaS Web App" service element to MONIT.

* https://cern.service-now.com/service-portal?id=kb_article&n=KB0002674
* https://monit-docs.web.cern.ch/metrics/http/#service-availability

# Status of the service

The script measures the availability of the service and sets it to three different values:

* *Unavailable*: If the OpenShift API cannot be contacted through a Route (veryfing HAProxy
keeps working)
* *Degraded*: If the memory available in Standard, Schedulable and Ready nodes is below
a given threshold. This means new apps would struggle to find room to run.
* *Available*: If all the checks aboves pass correctly.

## Additional metrics

In addition to the availability, there are a few metrics been sent to SLS.
These metrics are exported directly from the Prometheus instance in the Openshift cluster (see `config.py` for the query definitions).

* Number of schedulable pods
* Number of running pods
* Total CPU cores requested by applications
* Total memory in Mb requested by applications
* Actual memory usage of all pods

# Service XML file example

```xml
<serviceupdate xmlns="http://sls.cern.ch/SLS/XML/update">
  <timestamp>2015-07-14T12:07:47</timestamp>
  <data>
    <numericvalue name="number_of_schedulable_nodes">14</numericvalue>
    <numericvalue name="number_of_running_pods">501</numericvalue>
    <numericvalue name="total_cpu_requested">...</numericvalue>
    <numericvalue name="total_memory_requested">...</numericvalue>
    <numericvalue name="actual_memory_usage">...</numericvalue>
  </data>
  <id>PaaS</id>
  <status>available</status>
  <availabilityinfo>available</availabilityinfo>
</serviceupdate>
```

# Deployment

The deployment is handled in the [okd4-install repository](https://gitlab.cern.ch/paas-tools/okd4-install).

The application can also be run locally if the correct environment variables are set:

```sh
# set up Python virtual environment
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt

# obtain token for connecting to cluster
export OPENSHIFT_TOKEN="$(oc -n openshift-monitoring serviceaccounts get-token external-metrics-reader)"

# set appropriate parameters for connecting to the cluster
export OPENSHIFT_ENDPOINT=https://api.clu-jack-dev.okd.cern.ch
export PROMETHEUS_ENDPOINT=https://prometheus-k8s-openshift-monitoring.clu-jack-dev.cern.ch
export DRY_RUN=true

# run the application
python3 ./openshift_availability.py
```
